# -*- coding: utf-8 -*-
"""
Created on Fri Apr 27 14:36:07 2018
@author: jenm
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Oct 31 14:14:55 2017

@author: jenm
"""
# We are producing in this the 1-day rate for Dry Vans 
# for the seven chosen lanes.
import pandas
import datetime
import getpass
import sqlalchemy
import cx_Oracle
import numpy as np
from pandas.tseries.holiday import AbstractHolidayCalendar, Holiday
from pandas.tseries.holiday import USMemorialDay, USThanksgivingDay
from pandas.tseries.holiday import USLaborDay, nearest_workday, previous_friday
#%%
#Creating a list of lanes:
lanelist = [{'orig': 'CA_LAX', 'dest': 'WA_SEA'},{'orig': 'WA_SEA', 'dest': 'CA_LAX' },
         {'orig': 'CA_LAX', 'dest': 'TX_DAL'},{'orig': 'TX_DAL', 'dest': 'CA_LAX'},
         {'orig': 'IL_CHI', 'dest': 'GA_ATL'},{'orig': 'GA_ATL', 'dest': 'PA_PHI'},
         {'orig': 'PA_PHI', 'dest': 'IL_CHI'}]
#Creating list of equipment type (currently just for van, but we may wish to 
#extend this later to Flatbed or Reefer):
equiplist = ('V')
# Creating the trip type list.  Currently, we only do this for the maxmimum trip
# type. The impact is neglibible as all origin/distination pairs are further 
# apart than the minimum distance in this band.  However, this permits it to be
# extensible to other trip types later. The trip limits are stored in a dictionary.
triptype = ('7')
triplimits = {'7':(496,'binary_double_infinity')}
# Creating the query base.  All %s mark where a variable will pass.
sql = """
select dd.calendar_date as PICKUP_DATE, equip_type,
       pickups, drops, linehaul_amount, company_id,
       to_char(date_updated,  'DD-MON-YYYY HH24:MI:SS') AS DATE_UPDATED,
       used_miles, hazmat
       
       
from dw_admin.dim_date dd  LEFT JOIN dw_admin.ds_tri_rates tr
on (trunc(pickup_date) = trunc(calendar_date)
and dd.is_weekend_day = 'N'
and (dd.holiday_name IS NULL 
or dd.holiday_name NOT IN('Christmas', 'Thanksgiving Day','New Years Day', 'Labor Day', 'Memorial Day', 'Independence Day')))


where 
--filtering for 4 day lag
--    (dw_start_date <= (pickup_date + 4)
--    AND (dw_current_row_flag = 'Y' OR dw_end_date > pickup_date + 4))
-- limiting to rates cleared to enter RV product
--    AND 
    (in_csb_flag = 1)
--limiting to Spot Market Rates
  and (broker_payment_classification = 'BCS')
--limiting to Van only
  and equip_type = '%s'
  and (contributor_type >= 1) and (contributor_type <= 5)
-- eliminate Mexico  
  and (o_zip != '00000') and (d_zip != '00000') 
  and (o_zip is not null) and (d_zip is not null) 
  and (o_city is not null) and (d_city is not null) 
  and (o_county is not null) and (d_county is not null)
--eliminate nonsense linehauls  
  and (linehaul_amount > 0)
--eliminate nonsense mileage    
  and (used_miles > 0) 
  and (load_id is not null)
-- this date filter is for the 90 days back data submission.  Actual Index Rate code will run only one day, not a 90 day range.
-- therefore we need different logic here for production.
  and dd.calendar_date >= TO_DATE('09-MAY-11')
  and dd.calendar_date <= trunc(CURRENT_DATE)-5
--this part for trip type
  and used_miles >= %s
  and used_miles <= %s
--remove HazMat
 and (hazmat IS NULL or hazmat = 0)
--remove multiple pickups and drop-offs
and (pickups IS NULL or pickups <= 1)
 and (drops IS NULL or drops <= 1)

-- choose geography level #2=DSG, which displays as 'Mkt' in the UI for our 7 lanes
  and substr(o_zip, 1,3) in (select postal_prefix from dw_admin.ds_csb_ma_postal_prefix o_mpp 
          where market_area_id = '%s' and market_area_level <= 3 )
      and substr(d_zip, 1,3) in (select postal_prefix from dw_admin.ds_csb_ma_postal_prefix d_mpp 
          where market_area_id = '%s' and market_area_level <= 3 )
"""
# Importing the query and creating a dataframe:
def getRawRates(query, username, pwd):
    """
    @PARAMS: username: DWH username credentials
             pwd = DWH password associated with username
             query: string which is executable SQL query
    @REQUIRES: PICKUP_DATE, DATE_UPDATED as columns in query.   
    Creates dataframe of query results.'PICKUP_DATE' and 'DATE_UPDATED' are cast 
    to pandas datetime and all headers are made uppercase.
    @RETURNS  dfRaw; dataframe of series and type as follow --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    """
    conntype = 'oracle+cx_oracle://'
    connstring = conntype + username + ":" + pwd + "@pdxoradb3/dwhprod" 
    engine = sqlalchemy.create_engine(connstring)
    engine.connect
    
    dfRaw = pandas.read_sql(query, engine)
    dfRaw.columns = [i.upper() for i in dfRaw.columns]

    dfRaw.loc[:,'PICKUP_DATE'] = pandas.to_datetime(dfRaw.loc[:,'PICKUP_DATE'])
    dfRaw.loc[:, 'DATE_UPDATED'] = pandas.to_datetime(dfRaw.loc[:, 'DATE_UPDATED'])
    return dfRaw
# Cleaning the raw dataframe for a specific date range for a single 1-day 
# rate and adding the LPM column:
def makeRateBase(dfRaw, pdate):
    """
    @PARAMS: pdate; a date
    dfRaw; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    @REQUIRES: LINEHAUL_AMOUNT, USED_MILES
    Modifies raw dataframe by filtering for the dates for a single pickup date and adds the LPM column
    @RETURNS:  dfBaseRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    """
    dfRaw = dfRaw[:]
    dfPickupDateCut = dfRaw[dfRaw.PICKUP_DATE.dt.date==pdate]
    dfBaseRates = dfPickupDateCut[:]
    c = dfBaseRates.loc[:,'LINEHAUL_AMOUNT']/dfBaseRates.loc[:,'USED_MILES']
    c = c.rename('LPM')
    dfBaseRates = pandas.concat([dfBaseRates, c], axis =1)
    #The above three lines just create the LPM column.
    return dfBaseRates
# Finding the limits (+/- 2SD) for filtering outliers.
def findPerMileLimits(dfBaseRates):
    """
    @PARAMS: dfBaseRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    @REQUIRES: LPM
    Takes dataframe and computes +/- 2 standard deviations of the LPM column as limits.
    @RETURNS: limits, a dictionary of the high and low limits
    """
    lpm = dfBaseRates.loc[:,'LPM']
    lowlpm = lpm.mean() - 2*lpm.std(ddof=0)
    highlpm = lpm.mean() + 2*lpm.std(ddof=0)
    limits = {'lowLimit':lowlpm, 'highLimit':highlpm}
    return limits
# Cleaning rates of outliers by applying the limits to the base rates.
def cleanRateBase(dfBaseRates, limits):
    """
    @PARAMS: limits; dictionary of the high and low LPM limits
    dfBaseRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    @REQUIRES: limits, the dictionary of limits
               LPM
    Takes dataframe filters out values above and below the limits.
    @RETURNS: dfCleanedRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    """
    filter = True
    dfBaseRates = dfBaseRates[:]
    for key in limits.keys():
        if key == 'lowLimit':
            filter = filter & (dfBaseRates.loc[:,'LPM'] >= limits['lowLimit'])
        if key == 'highLimit':
            filter = filter & (dfBaseRates.loc[:,'LPM'] <= limits['highLimit'])
    dfCleanedRates = dfBaseRates.loc[filter]
    return dfCleanedRates
# Removing HazMat labeled rates:
def cutHazMatRates(dfCleanedRates):
    """
    @PARAMS: dfCleanedRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    @REQUIRES: HAZMAT
    Filters dataframe to remove rates flagged as 'HazMat'.
    @RETURNS:  dfNoHazMatRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    """
    filter = True
    dfCleanedRates = dfCleanedRates[:]
    filter = filter & (dfCleanedRates.loc[:, 'HAZMAT'] != 1)
    dfNoHazMatRates = dfCleanedRates.loc[filter]
    return dfNoHazMatRates
# Removing rates with more than one pickup:
def cutMultiPickup(dfNoHazMatRates):
    """
    @PARAMS: dfNoHazMatRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    @REQUIRES: PICKUPS
    Fills in Null values in PICKUPS for input dataframe.
    Filters input dataframe to remove rows where the value in this series is greater than 1.
    @RETURNS:  dfSinglePickupRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    """
    filter = True
    dfNoHazMatRates = dfNoHazMatRates[:]
    dfNoHazMatRates.loc[:,'PICKUPS']=dfNoHazMatRates.loc[:,'PICKUPS'].fillna(0)
    filter = filter & (dfNoHazMatRates.loc[:, 'PICKUPS'] <= 1)
    dfSinglePickupRates = dfNoHazMatRates.loc[filter]
    return dfSinglePickupRates
# Removing rates with more than one drop-off:
def cutMultiDrop(dfSinglePickupRates):
    """
    @PARAMS: dfSinglePickupRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    @REQUIRES: DROPS
    Fills in Null values in DROPS for input dataframe.
    Filters input dataframe to remove rows where the value in this series is greater than 1.
    @RETURNS:  dfSingleDropRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    """
    filter = True
    dfSinglePickupRates = dfSinglePickupRates[:]
    dfSinglePickupRates.loc[:,'DROPS']=dfSinglePickupRates.loc[:,'DROPS'].fillna(0)
    filter = filter & (dfSinglePickupRates.loc[:, 'DROPS'] <= 1)
    dfSingleDropRates = dfSinglePickupRates.loc[filter]
    return dfSingleDropRates
# Checking for sufficient contributions (3&8 rule):
def checkRateSufficiency(dfSingleDropRates):
    """
    @PARAMS: dfSingleDropRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    @REQUIRES: COMPANY_ID
    Counts the number of unique companies in COMPANY_ID and the length of the dataframe.
    Checks to make sure the unique company count is at least 3 and the row count is at least 8.
    @RETURNS: Boolean
    """
    dfSingleDropRates = dfSingleDropRates[:]
    comp_cnt = len(dfSingleDropRates['COMPANY_ID'].unique())
    report_cnt = len(dfSingleDropRates['COMPANY_ID'])
    if (comp_cnt >= 3 and report_cnt >=8):
        return True
    elif (report_cnt < 8 or comp_cnt < 3):
        return False
# Finding the overall average LPM and the LPM per company id:
def calcPerMileAverages(dfSingleDropRates):
    """
    @PARAMS: dfSingleDropRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    @REQUIRES: LINEHAUL_AMOUNT, USED_MILES, COMPANY_ID
    Computes the average LPM and the LPM per unique company. Returns these averages in a dictionary.
    @RETURNS:  lpmAverages, a dictionary of the overall and the per company linehaul per mile average.
    """
    dfSingleDropRates = dfSingleDropRates[:]
    simple_avg = dfSingleDropRates['LINEHAUL_AMOUNT'].sum()/dfSingleDropRates['USED_MILES'].sum()
    dfPerCompany = dfSingleDropRates.groupby(['COMPANY_ID']).sum()
    dfPerCompany.loc[:,'avg'] = dfPerCompany.loc[:,'LINEHAUL_AMOUNT'] / dfPerCompany.loc[:,'USED_MILES']
    avg_of_company_avg = dfPerCompany['avg'].mean()
    lpmAverages = {'simple_avg':simple_avg,'avg_of_company_avg':avg_of_company_avg}   
    return lpmAverages
# Calculating the weighted average:
def calcPerMileWeightAverage(dfSingleDropRates, lpmAverages):
    """
    @PARAMS: lpmAverages; dictionary containing the overall average LPM and the per company average LPM.
    dfSingleDropRates; dataframe with series and types as follows --
    PICKUP_DATE        datetime64[ns]
    EQUIP_TYPE                 object
    PICKUPS                   float64
    DROPS                     float64
    LINEHAUL_AMOUNT           float64
    COMPANY_ID                  int64
    DATE_UPDATED       datetime64[ns]
    USED_MILES                  int64
    HAZMAT                    float64
    LPM                       float64
    @REQUIRES: COMPANY_ID
    Calculates the unique company count. Uses this count to determine the contributor weight.
    @RETURNS: weighted_avg  float64
    """
    dfSingleDropRates = dfSingleDropRates[:]
    comp_cnt = len(dfSingleDropRates['COMPANY_ID'].unique())
    contributor_weight = float(comp_cnt) / ((comp_cnt - 1) * 2)
    weighted_avg = lpmAverages['avg_of_company_avg'] * contributor_weight + (1 - contributor_weight) * lpmAverages['simple_avg']
    return weighted_avg

class TradingCalendar(AbstractHolidayCalendar):
    rules = [USMemorialDay, USThanksgivingDay,USLaborDay,
        Holiday('Christmas', month=12, day=25, observance=nearest_workday),
        Holiday('New Years Day', month=1, day=1, observance=nearest_workday),
        Holiday('July 4th', month=7, day=4, observance=nearest_workday)
    ]

cal = TradingCalendar()


def get_Holidays(start_date, end_date):
    dr = pandas.date_range(start  = start_date, end = end_date)
    holidays = cal.holidays(start=dr.min(), end=dr.max())
    return holidays

    
# Putting them all together to iterate over the lanes, equipment, and pickup dates:
def rateHist90Days(lanelist,equiplist,triptype,triplimits,sql):
    """
    @PARAMS: None
    @REQUIRES: lanelist,equiplist,triptype, triplimits,sql
    Iteratively builds a query for a specific equipment, origin, and destination from a lane in lanelist 
    and an equipemnt in equiplist for a trip type in triplist. Creates dataframe from this query.Performs rate calulation for each pickup date.
    @RETURNS: dfIndexRates, dataframe with series and types as follows --
    CA_LAX_to_WA_SEA_by_V    object
    WA_SEA_to_CA_LAX_by_V    object
    CA_LAX_to_TX_DAL_by_V    object
    TX_DAL_to_CA_LAX_by_V    object
    IL_CHI_to_GA_ATL_by_V    object
    GA_ATL_to_PA_PHI_by_V    object
    PA_PHI_to_IL_CHI_by_V    object
    """
    username =  raw_input("Please enter your Data Warehouse user name: ")
    pwd = getpass.getpass("Please enter your Data Warehouse password: ")
    lanes = lanelist
    eq = equiplist
    tr = triptype
    dfIndexRates = pandas.DataFrame()
    dfExtrapRates = pandas.DataFrame(columns = ['PDATE', 'LANE', 'LPM'])
    for lane in lanes:
        ukey = lane['orig'] + '_to_' + lane['dest']
        for item in eq:
            for trip in tr:
                ukey += '_by_' + item
                query = sql % (item, triplimits[trip][0], triplimits[trip][1], lane['orig'], lane['dest'])
                dfRaw = getRawRates(query, username, pwd)
                datelist = np.sort(dfRaw.PICKUP_DATE.unique())
                min_date_range = datelist.min()
                max_date_range = datelist.max()
                holidays = get_Holidays(min_date_range, max_date_range)
                data = {}
                for pudate in datelist:
                    pdate = pandas.to_datetime(pudate).date()
                    dfBaseRates = makeRateBase(dfRaw, pdate)
                    limits = findPerMileLimits(dfBaseRates)
                    dfCleanedRates = cleanRateBase(dfBaseRates, limits)
                    dfNoHazMatRates = cutHazMatRates(dfCleanedRates)
                    dfSinglePickupRates = cutMultiPickup(dfNoHazMatRates)
                    dfSingleDropRates = cutMultiDrop(dfSinglePickupRates)
                    rate_cnt = len(dfSingleDropRates)
                    comp_cnt = len(dfSingleDropRates['COMPANY_ID'].unique())
                    if checkRateSufficiency(dfSingleDropRates) == False:
                        try:
                            #data[str(pdate)] = 'Insufficient Rates: ' + str(rate_cnt) + ' rates from ' + str(comp_cnt) + ' companies.'
                            if pdate.weekday()==0:
                                days_back = datetime.timedelta(days=3)
                            else:
                                days_back = datetime.timedelta(days=1)
                            prior_date = pdate - days_back
                            if prior_date in holidays:
                                prior_date = previous_friday(prior_date - datetime.timedelta(days=1))                         
                            if str(prior_date) in data:
                                prior_date
                            else:
                                prior_date = previous_friday(prior_date - datetime.timedelta(days=1))
                            extrap_rate = 0
                            if comp_cnt < 3:
                                extrap_rate = extrap_rate + (3-comp_cnt)
                            else:
                                extrap_rate = extrap_rate + 0
                            if rate_cnt < 8:
                                extrap_rate = extrap_rate + (8-rate_cnt)
                            else:
                                extrap_rate = extrap_rate + 0
                            rate_ratio = rate_cnt/(rate_cnt+extrap_rate)
                            extrap_average = (dfSingleDropRates['LPM'].mean()*rate_ratio)+(data[str(prior_date)]*(1-rate_ratio))
                            data[str(pdate)] =extrap_average
                            print ("Rate extrapolation on "+str(pdate)+" for "+ukey)
                            row = {'PDATE':str(pdate), 'LANE': ukey, 'LPM':extrap_average}
                            dfExtrapRates = dfExtrapRates.append(row, ignore_index=True)
                        except Exception as E:
                            print(E)
                            print(pdate)
                            print(prior_date)
                            print(ukey)
                    else:
                        lpmAverages = calcPerMileAverages(dfSingleDropRates)
                        weighted_avg = calcPerMileWeightAverage(dfSingleDropRates, lpmAverages)
                        data[str(pdate)] = weighted_avg
                col = pandas.Series(data)
                dfIndexRates[ukey]=col
    username = str("null")
    pwd = str("null")
    return dfIndexRates, dfExtrapRates
# Running the proces:
dfIndexRates, dfExtrapRates = rateHist90Days(lanelist,equiplist,triptype,triplimits,sql)
dfIndexRates = dfIndexRates.fillna("Insufficient Rates: 0 rates from 0 companies.")
dfIndexRates.to_csv('DAT_Index_Full_Subject_to_Revision_extrapolated.csv')
dfExtrapRates.to_csv('DAT_Index_Full_Subject_to_Revision_extrapolated_rates.csv', index = False)



#%%
